package br.ucsal.bes20202.bd2.clinica;

import java.time.LocalDate;
import java.util.List;

public class Funcionario {

	private Integer matricula;

	private Integer numeroRg;

	private String orgaoExpedidorRg;

	private UF ufRg;

	private LocalDate dataAdmissao;

	private String email;

	private List<String> telefones;

	public Funcionario() {
	}

	public Funcionario(Integer matricula, Integer numeroRg, String orgaoExpedidorRg, UF ufRg, LocalDate dataAdmissao,
			String email, List<String> telefones) {
		this.matricula = matricula;
		this.numeroRg = numeroRg;
		this.orgaoExpedidorRg = orgaoExpedidorRg;
		this.ufRg = ufRg;
		this.dataAdmissao = dataAdmissao;
		this.email = email;
		this.telefones = telefones;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public Integer getNumeroRg() {
		return numeroRg;
	}

	public void setNumeroRg(Integer numeroRg) {
		this.numeroRg = numeroRg;
	}

	public String getOrgaoExpedidorRg() {
		return orgaoExpedidorRg;
	}

	public void setOrgaoExpedidorRg(String orgaoExpedidorRg) {
		this.orgaoExpedidorRg = orgaoExpedidorRg;
	}

	public UF getUfRg() {
		return ufRg;
	}

	public void setUfRg(UF ufRg) {
		this.ufRg = ufRg;
	}

	public LocalDate getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(LocalDate dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataAdmissao == null) ? 0 : dataAdmissao.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		result = prime * result + ((numeroRg == null) ? 0 : numeroRg.hashCode());
		result = prime * result + ((orgaoExpedidorRg == null) ? 0 : orgaoExpedidorRg.hashCode());
		result = prime * result + ((telefones == null) ? 0 : telefones.hashCode());
		result = prime * result + ((ufRg == null) ? 0 : ufRg.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (dataAdmissao == null) {
			if (other.dataAdmissao != null)
				return false;
		} else if (!dataAdmissao.equals(other.dataAdmissao))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (numeroRg == null) {
			if (other.numeroRg != null)
				return false;
		} else if (!numeroRg.equals(other.numeroRg))
			return false;
		if (orgaoExpedidorRg == null) {
			if (other.orgaoExpedidorRg != null)
				return false;
		} else if (!orgaoExpedidorRg.equals(other.orgaoExpedidorRg))
			return false;
		if (telefones == null) {
			if (other.telefones != null)
				return false;
		} else if (!telefones.equals(other.telefones))
			return false;
		if (ufRg == null) {
			if (other.ufRg != null)
				return false;
		} else if (!ufRg.equals(other.ufRg))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Funcionario [matricula=" + matricula + ", numeroRg=" + numeroRg + ", orgaoExpedidorRg="
				+ orgaoExpedidorRg + ", ufRg=" + ufRg + ", dataAdmissao=" + dataAdmissao + ", email=" + email
				+ ", telefones=" + telefones + "]";
	}

}
